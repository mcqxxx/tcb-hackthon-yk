## CODEADM 服务简介

​	codeadm 是一个轻量级的NodeJs服务程序，主要是为[疫情控制](https://gitee.com/mcqxxx/tcb-hackthon-yk)系统服务提供的二维码服务，是整个系统的组成部分之一。

​	具有独立部署，启动迅速，占有资源少等优点，适合对二维码服务要求简单，快速应用的场景，提供配套服务。

## 功能简介

​    主要提供对二维码一些服务，主要配合微信服务程序（例如微信小程序）等，对前后端提供支持，主要以API接口方式实现。

### 1、二维码生成：

​	本功能是接受请求，根据不同的用户ID，产生不同的系统二维码的代码标识。

### 2、二维码验证：

   验证二维码的代码是否属于本系统生成，且返回必要信息。

### 3、用户登录:

​	用户登录功能，确认该用户是可授信的，可使用本服务。



##  部署

​    部署建议在服务器上按照NodeJs，推荐使用Linux 服务操作系统。

  1、安装NodeJs环境 ,  下载具体详见：https://nodejs.org/en/download/，需要NodeJs 8.0以上的版本。

  2、下载源代码。

  3、在codeamd目录下，执行npm install ，安装Nodejs程序执行依赖。

  4、在codeamd/config 下，对服务器配置文件进行修改。

  5、执行服务系统程序，nodejs app.js &。

​      推荐采用pm2管理器对服务进行管理，详见：https://pm2.keymetrics.io/。

##  

## API 接口说明

​	本服务提供HTTP API接口，遵循RESTful 规则，采用JSON作为数据报文格式。 

####    1、二维码生成：

​        path： /api/qrcode，method：POST   

​		参数： openId, invalidTime

​		示范： curl -H "Content-Type:application/json" -H "Data_Type:msg" -X POST --data '{"openId": "test000000001", "invalidTime": 2}' http://127.0.0.1:8088/api/qrcode

####    2、二维码验证：

​      path：/api/qrcode/check，method：GET

​		参数：code 

​        示范：curl http://127.0.0.1:8088/api/qrcode/check?code=A21132DEC2131E9

####    3、用户登录:

​       path：/api/login，method：POST   

​		参数：openId，unionId，mobile。。。。等用户信息

​		示范：curl -H "Content-Type:application/json" -H "Data_Type:msg" -X POST --data '{"openId": "test000000001", "mobile": "18000218888", "unionId": "12121212" }'  http://127.0.0.1:8088/api/login



## 更新日志sd