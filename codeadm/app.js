'use strict';
const restify = require('restify');
const restifyPlugin = restify.plugins;

const qrcode = require('./api/qrcode'); 
const log =  require('./lib/log4js')();
const user = require('./api/user');

const server = restify.createServer();

server.use(restifyPlugin.jsonp());
//解析HTTP查询字符串
server.use(restifyPlugin.queryParser());
//请求数据转换为JavaScript对象

server.use(restifyPlugin.bodyParser());

server.use(function(req, res, next) {
  var body = req.body;
  if (req.route.method == 'GET'){
    body = req.query;
  };
  log.info('api req:' + req.route.method + ', path:' + req.route.path + ', body:' + JSON.stringify(body));
  return next();
});

server.get('/', function(req, res, next) {
  res.send('hello!');
});

server.post('/api/qrcode',function(req, res, next) {
  qrcode.setCode(req, res);
});

server.get('/api/qrcode/check',function(req, res, next) {
  qrcode.checkCode(req, res);
});

server.post('/api/login', function(req, res, next) {
  user.login(req, res);
});

server.listen(8088, function() {
  console.log('%s listening at %s', server.name, server.url);
});
