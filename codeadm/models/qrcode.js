'use strict';
const Sequelize = require('sequelize');
const db = require('./mysql');

//信息模型
module.exports = db.sequelize.define('ca_qrcode', {
  orderNo: { type: Sequelize.STRING(32) },
  code: { type: Sequelize.STRING(96) },
  type: { type: Sequelize.INTEGER },
  openId:  { type: Sequelize.STRING(32) },
  lon: { type: Sequelize.DOUBLE }, //经度：longitude
  lat: { type: Sequelize.DOUBLE }, //纬度：latitude
  accode: {type: Sequelize.STRING(6)}, //城市代码 
  status: { type: Sequelize.INTEGER },  //1: 生效， 2：无效
  attach: { type: Sequelize.STRING(255) },
  invalidTimestamp: { type: Sequelize.INTEGER },
  createTime: { type: Sequelize.STRING(20) }
}, {
  freezeTableName: true,  // Model tableName will be the same as the model name
  timestamps: true,       // 是否需要增加createdAt、updatedAt、deletedAt字段
  deletedAt: false
});