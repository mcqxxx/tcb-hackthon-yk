'use strict';
const Sequelize = require('sequelize');
const db = require('./mysql');

//信息模型
module.exports = db.sequelize.define('ca_user', {
  openId:  { type: Sequelize.STRING(32) },
  unionId:  { type: Sequelize.STRING(32) },
  name: { type: Sequelize.STRING(16) },
  nickName:  { type: Sequelize.STRING(32) },
  source: { type: Sequelize.STRING(64) }, //来源
  env: { type: Sequelize.STRING(64) },
  type: { type: Sequelize.INTEGER }, //  1、超级用户，2、高级管理、 3、一般管理者、4、普通用户
  mobile: { type: Sequelize.STRING(20) },
  role:  { type: Sequelize.INTEGER }, //权限大小
  detail: {type: Sequelize.STRING(36)}, //描述
  accode: {type: Sequelize.STRING(6)}, //城市代码 
  status: { type: Sequelize.INTEGER },  //1: 生效， 0：无效
  attach: { type: Sequelize.STRING(255) },
  loginCount: { type: Sequelize.INTEGER },
  loginTime: { type: Sequelize.STRING(20) },
  createTime: { type: Sequelize.STRING(20) }
}, {
  freezeTableName: true,  // Model tableName will be the same as the model name
  timestamps: true,       // 是否需要增加createdAt、updatedAt、deletedAt字段
  deletedAt: false
});