'use strict';

const moment = require('moment');
const encrypt = require('./encrypt');

function getQrcode(data){
  var text = data.openId + moment().format('x');
  return encrypt.sha256(text);
};

module.exports = {
  getQrcode: getQrcode
};