'use strict';

const crypto = require('crypto');

function sha256(data) {
  if (data != null && typeof(data) == 'string' && data != ''){
    return crypto.createHash('sha384').update(data).digest('hex').toUpperCase();
  }else{
    return "";
  };
};

module.exports = {
  sha256: sha256
}