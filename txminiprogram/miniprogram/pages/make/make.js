// pages/make/make.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    empty: false,
    isLoading: true,
    isLogin: false,
    showRule: false,
    upload_auth: 0,
    photo_id: "",
    userID: "",
    newName: "",
    page: 0,
    perpage: 10,
    timelineList: [],
    frist: false,          //第一次上传提示
    disable: false,
    disable2: false,      
    is_admin: false,
    allow_comment: 0,     // 0禁用 | 1启用
    users: [],
    type: 0,
    isUpload: false,
    plan: 0,
    uploadPlan: 0,
    uploadLength: 0,
    wallList: [],
    from_time: "",
    to_time: "",
    options: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

    console.log('onLoad')
    
    //这个页面需要登录

    this.setData({
      options: options,
      allow_comment: app.globalData.allow_comment
    })

    this.login = this.selectComponent("#login");

    this.loader = this.selectComponent("#loader");

    this.loader.showLoader()

    this.login.hideLogin()

    this.loader.hideLoader()
    
  },
 

   //取消事件 
  _cancelEvent(){
    wx.showToast({
      title: '登录取消',
      icon: 'none',
      duration: 1000
    })
    this.login.hideLogin()
  }, 
  //确认事件
  _confirmEvent(){

    wx.showToast({
      title: '登录成功',
      icon: 'success',
      duration: 1000
    })

    //判断options
    let options = this.data.options
    let photo_id = options.photo_id
    let invite_user = options.invite_user

    if (!app.isNull(photo_id)) {
      //分享进入  
      this.initLoad()
    } else {
      //普通进入
      this.initShow()
    }

    this.login.hideLogin()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  closeFrist(){
    this.setData({
      frist: false
    })
  },

  twoDecimal(oNum) {
    var num = parseFloat(oNum);
    var num = Math.round(oNum * 100) / 100;
    return num;
  },

  //编辑相册
  editCover(){
    wx.navigateTo({
      url: '/pages/cover/cover'
    })
  },

  back() {

    let pages = getCurrentPages()

    if (pages.length > 1) {

      let _target = 0, _target2 = 0

      for(let i=0; i<pages.length; i++){
        if (pages[i].route == 'pages/index/index'){
          _target = i
          break
        }
      }

      _target2 = pages.length - _target - 1
    
      if (_target2 != 0) {
        //返回至首页
        wx.navigateBack({
          delta: _target2,
        })
      } else {
        //跳转至首页
        wx.navigateTo({
          url: '/pages/index/index',
        })
      }
    } else {
      //跳转至首页
      wx.navigateTo({
        url: '/pages/index/index',
      })
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  onShareAppMessage(e) {

    if (e.from == 'button') {

      console.log('/pages/join/join?dynamic_id=' + e.target.dataset.id + '&invite_user=' + e.target.dataset.user + '&isShare=true')

      //分享心情  
      return {
        title: e.target.dataset.emotion || e.target.dataset.username + '分享了他的照片，快来看看吧',
        path: '/pages/join/join?dynamic_id=' + e.target.dataset.id + '&invite_user=' + e.target.dataset.user + '&isShare=true',
        imageUrl: e.target.dataset.img + '?x-oss-process=image/resize,m_fill,h_500,w_400'
      }
    } else {
      //自带的分享
    }

  },

})