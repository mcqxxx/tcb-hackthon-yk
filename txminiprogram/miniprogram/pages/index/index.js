//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    isLogin: false,
    list: [1, 2, 3]
  },

  onLoad() {
    this.login = this.selectComponent("#login");
    this.login.showLogin()
  },

  onShow() {

  },

  formSubmit(e){
  },

  //取消事件 
  _cancelEvent(){
    wx.showToast({
      title: '登录取消',
      icon: 'none',
      duration: 1000
    })
    this.setData({
      isLogin: false,
    })
    this.login.hideLogin()
  }, 
  //确认事件
  _confirmEvent(){

    let _this = this

    wx.showToast({
      title: '登录成功',
      icon: 'success',
      duration: 1000
    })

    this.setData({
      isLogin: true,
    })

    this.login.hideLogin()
  },

  //跳传相册详情
  goAlbum(val) {

    wx.navigateTo({
      url: '/pages/make/make'
    })

  },

})
