// 云函数入口文件
const cloud = require('wx-server-sdk');
const request = require('request');
const uuid = require('node-uuid');

const url = "http://47.114.59.59:8088/api/qrcode"

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {

  const wxContext = cloud.getWXContext()
  var requestData = {
    orderNo: uuid.v1(),
    openId: wxContext.OPENID,
    lon: event.lon,
    lat: event.lat,
    invalidTime: event.invalidTime
  };
  request({
    url: url,
    method: "POST",
    json: true,
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify(requestData)
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // 请求成功的处理逻辑
      return {
        error: null,
        code: body.code,
        openid: wxContext.OPENID
      };
    } else {
      return {
        error: error
      };
    };
  });

}