// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async(eve// 云函数入口文件
const cloud = require('wx-server-sdk')
const request = require('request');

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {

  const wxContext = cloud.getWXContext()
  const db = cloud.database();

  if (typeof (event.lon) !== 'Number') {
    event.lon = 90;
  };
  if (typeof (event.lat) !== 'Number') {
    event.lat = 0;
  };

  /************自身数据情况 */
  var data = {
    env: wxContext.ENV,
    unionId: wxContext.UNIONID,
    source: wxContext.SOURCE,
    openId: wxContext.OPENID,
    name: event.name, //微信名
    nick: event.nick, //微信昵称
    mobile: event.mobile,
    temperature: event.temperature,
    location: new db.Geo.Point(event.lon, event.lat),
    createTime: new Date(),
    status: true
  };

  const url = "http://47.114.59.59:8086/api/note";

  db.collection('note').add({
    data: data
  }).then(res => {
    console.log('create info:', res);
    data.orderNo = res._id;
    /**********数据上传 */
    request({
      url: url,
      method: "POST",
      json: true,
      headers: {
        "content-type": "application/json",
      },
      body: data
    }, function (error, response, body) {
      console.log('body', response, body);
      if (!error && response.statusCode == 200) {
        return {
          error: null,
          creatTime: body.createTime
        };
      } else {
        return {
          error: error
        };
      };
    });

  });

}