'use strict'

const log = require('../lib/log4js')();

module.exports = function(){

  return function (req, res, next){
    
    var body = null;
    var method = req.method.toLowerCase();
    var path = req.path;
    //请求类型判断
    if ( method == "post" || method == "put" ) {
      body = req.body;
    }else{
      body = req.query;
    };
    log.info("requst " + method + ", path:" + req.path + ", body:" + JSON.stringify(body));
    next();

  };

};