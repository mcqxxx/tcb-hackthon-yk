'use strict'

const record = require('../web/record');
const info = require('../api/info');
const note = require('../api/note');
const preprocess = require('./preprocess');

exports.all = function( app ){

  console.log('ready');

  app.use(preprocess());

  /************* 访问用户身份验证 ***************/

  app.head('/', function(req, res){
    res.send('hello world!'); 
  });
  
  //扫码进入页面,获取信息
  app.get('/web/record', function(req, res) {
    record.getRecord(req, res);
  });

  //记录上送异常信息
  app.post('/api/info', function (req, res) {  
    info.create(req, res);
  });

  //记录用户记录
  app.post('/api/note', function (req, res) {  
    note.create(req, res);
  });

};