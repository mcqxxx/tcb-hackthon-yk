'use strict'

const cronJob = require("cron").CronJob;
const log = require('./lib/log4js')();
const moment = require('moment');

const infoDb = require('./models/info');
const recordDb = require('./models/record');

/****************** 获取token任务, 每一个小时执行一次 ***********************/
var jobCheck = new cronJob('0 55 * * * * ', function () {
  checkInfo();
}, null, false, 'Asia/Shanghai');

async function checkInfo(){

  try{

    var records = [];
    var checkTimestamp = Number(moment().format('X')) - 3600; //一小时内数据

    var list = await infoDb.findAll({where: {timestamp: {$lt: checkTimestamp} }});

    for(let i = 0; i < list.length; i++){

      let openId = list[i].openId;

      let data = await recordDb.findAll({where: {openId: openId}});
      
      for(let j = 0; j < data.length; j++){
        records.push({
          code: data[j].code,
          openId: openId,
          createTime: data[j].createTime,
          adminOpenId: data[j].adminOpenId
        });
      };

    };

    /***********记录需要通知小程序用户 或者 管理员用户*/


  }catch(error){
    log.error('error', error);
  };
  
};

begin();

function begin() {
  jobCheck.start();
};