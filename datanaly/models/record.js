'use strict';
const Sequelize = require('sequelize');
const db = require('./mysql');

//扫码获取记录
module.exports = db.sequelize.define('da_record', {
  openId:  { type: Sequelize.STRING(32) },
  unionId: { type: Sequelize.STRING(32) },
  source: { type: Sequelize.STRING(32) },
  env: { type: Sequelize.STRING(24) },
  accode: {type: Sequelize.STRING(6)}, //城市代码 
  code: { type: Sequelize.STRING(96) }, //二维码
  name: { type: Sequelize.STRING(24) },
  nickName: { type: Sequelize.STRING(24) },
  mobile:  { type: Sequelize.STRING(20) },
  status:  { type: Sequelize.INTEGER },
  codefLon: { type: Sequelize.DOUBLE }, //扫码经度：longitude
  codeLat: { type: Sequelize.DOUBLE }, //扫码纬度：latitude
  selfLon: { type: Sequelize.DOUBLE }, //当前经度：longitude
  selfLat: { type: Sequelize.DOUBLE }, //当前纬度：latitude
  adminOpenId: { type: Sequelize.STRING(32) },
  attach: { type: Sequelize.STRING(255) },
  createTime: { type: Sequelize.STRING(20) }
}, {
  freezeTableName: true,  // Model tableName will be the same as the model name
  timestamps: true,       // 是否需要增加createdAt、updatedAt、deletedAt字段
  deletedAt: false
});