'use strict';
const Sequelize = require('sequelize');
const db = require('./mysql');

//上送异常信息模型
module.exports = db.sequelize.define('da_info', {
  orderNo: { type: Sequelize.STRING(32) },
  openId:  { type: Sequelize.STRING(32) },
  unionId: { type: Sequelize.STRING(32) },
  source: { type: Sequelize.STRING(32) },
  env: { type: Sequelize.STRING(24) },
  accode: {type: Sequelize.STRING(6)}, //城市代码 
  sex: { type: Sequelize.INTEGER }, // 1、男，2、女
  age:  { type: Sequelize.INTEGER },
  name: { type: Sequelize.STRING(24) },
  nickName: { type: Sequelize.STRING(24) },
  mobile:  { type: Sequelize.STRING(20) },
  temperature:  { type: Sequelize.FLOAT },
  identityId: { type: Sequelize.STRING(20) },
  identityType: { type: Sequelize.INTEGER },
  timestamp: { type: Sequelize.INTEGER }, //时间戳，发生时间
  status:  { type: Sequelize.INTEGER },
  result: { type: Sequelize.STRING(24) },  //结果状态
  img: { type: Sequelize.STRING(64) }, //url
  lon: { type: Sequelize.DOUBLE }, //经度：longitude
  lat: { type: Sequelize.DOUBLE }, //纬度：latitude
  adminOpenId: { type: Sequelize.STRING(32) }, //管理员openID 
  attach: { type: Sequelize.STRING(255) },
  createTime: { type: Sequelize.STRING(20) }
}, {
  freezeTableName: true,  // Model tableName will be the same as the model name
  timestamps: true,       // 是否需要增加createdAt、updatedAt、deletedAt字段
  deletedAt: false
});
