'use strict';
const Sequelize = require('sequelize');
const db = require('./mysql');

//记录信息模型
module.exports = db.sequelize.define('da_note', {
  orderNo: { type: Sequelize.STRING(32) },
  openId:  { type: Sequelize.STRING(32) },
  unionId: { type: Sequelize.STRING(32) },
  source: { type: Sequelize.STRING(32) },
  env: { type: Sequelize.STRING(24) },
  accode: {type: Sequelize.STRING(6)}, //城市代码 
  mobile:  { type: Sequelize.STRING(20) },
  temperature:  { type: Sequelize.FLOAT },
  status:  { type: Sequelize.INTEGER },
  result: { type: Sequelize.STRING(24) },  //结果状态
  lon: { type: Sequelize.DOUBLE }, //经度：longitude
  lat: { type: Sequelize.DOUBLE }, //纬度：latitude
  attach: { type: Sequelize.STRING(255) },
  createTime: { type: Sequelize.STRING(20) }
}, {
  freezeTableName: true,  // Model tableName will be the same as the model name
  timestamps: true,       // 是否需要增加createdAt、updatedAt、deletedAt字段
  deletedAt: false
});