'use strict';

const moment = require('moment');
const noteDb = require('../models/note');
const log =  require('../lib/log4js')();

async function create(req, res){

  try{

    var body = req.body;
    
    var createTime = moment().format('YYYY-MM-DD HH:mm:ss');
    var ret = await noteDb.create({
      orderNo: body.orderNo,
      openId: body.openId,
      source: body.source,
      env: body.env,
      accode: body.accode,
      temperature: body.temperature,
      mobile: body.mobile,
      status: 1,
      lon: body.lon,
      lat: body.lat,
      createTime: createTime
    });

    res.send({
      error: null,
      createTime: createTime
    });

  }catch(error){
    log.error('api res:' + req.route.path + ' res error: ' + error.toString());
    res.send({error: error});
  };

};

module.exports = {
  create: create
};