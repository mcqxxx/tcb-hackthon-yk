'use strict';

const moment = require('moment');
const infoDb = require('../models/info');
const log =  require('../lib/log4js')();

async function create(req, res){

  try{

    var body = req.body;
    
    var createTime = moment().format('YYYY-MM-DD HH:mm:ss');

    if (body.timestamp == null){
      timestamp = moment().format('X');
    };

    var ret = await infoDb.create({
      orderNo: body.orderNo,
      openId: body.openId,
      source: body.source,
      env: body.env,
      sex: body.sex,
      age: body.age,
      accode: body.accode,
      name: body.name,
      nickName: body.nickName,
      mobile: body.mobile,
      temperature: body.temperature,
      identityId: body.identityId,
      identityType: body.identityType,
      timestamp: body.timestamp,
      status: 1,
      result: body.result,
      lon: body.lon,
      lat: body.lat,
      img: body.img,
      adminOpenId: body._openId,
      createTime: createTime
    });

    res.send({
      error: null,
      createTime: createTime
    });


  }catch(error){
    log.error('api res:' + req.route.path + ' res error: ' + error.toString());
    res.send({error: error});
  };

};

module.exports = {
  create: create
}