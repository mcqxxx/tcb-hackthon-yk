'use strict';
const log4js = require( 'log4js' );

const config = {
  appenders: {
    data : {
      "type": "dateFile",
      "filename":"./logs/",
      "pattern": "yyyyMMdd.log",
      "absolute":true,
      "alwaysIncludePattern":true
    },
    sql: {
      "type": "dateFile",
      "filename":"./logs/sql/",
      "pattern": "yyyyMMdd.log",
      "absolute":true,
      "alwaysIncludePattern":true
    }
  },
  //replaceConsole: false,
  categories: {
    default: { appenders: ['data',], level: 'ALL' },
    data: { appenders: ['data',], level: 'ALL' },
    sql: { appenders: ['sql',], level: 'ALL' }
  },
  pm2: true,
  pm2InstanceVar: 'INSTANCE_ID'
};

log4js.configure( config );

module.exports = function (type) {
  var logger = log4js.getLogger(type);
  return logger;
};