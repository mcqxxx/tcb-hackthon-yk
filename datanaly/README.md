## datanaly 服务简介

​	datanaly 是一个轻量级的NodeJs服务程序，主要是为[疫情控制](https://gitee.com/mcqxxx/tcb-hackthon-yk)系统服务提供的数据处理服务，是整个系统的组成部分之一。

​	具有独立部署，启动迅速，占有资源少等优点，支持微信小程序特定场景的服务，快速应用的场景，提供配套服务。

## 功能简介

​    主要提供对数据上送服务和简单数据的处理服务，主要配合微信服务程序（例如微信小程序）等，对前后端提供支持，主要以API接口方式实现。

### 1、信息上送：

​	本功能是接受信息的上送请求，将异常数据进行保存。

### 2、数据登记：

   提供为小程序的用户提供数据记录的存储，提供信息记录服务。

### 3、接受记录：

​	接受小程序的二维码访问页面功能，记录定点管理的二维码信息的用户数据记录，进行保存。

### 4、数据解析服务

​	对上送的异常信息和接受的定点管离的记录数据进行比对，分析出需要关注的信息数据，并且根据需要进行推送给小程序端。

##  部署

​    部署建议在服务器上按照NodeJs，推荐使用Linux 服务操作系统。

  1、安装NodeJs环境 ,  下载具体详见：https://nodejs.org/en/download/，需要NodeJs 8.0以上的版本。

  2、下载源代码。

  3、在datanaly目录下，执行npm install ，安装Nodejs程序执行依赖。

  4、在datanaly/config 下，对服务器配置文件进行修改。

  5、执行服务系统程序，nodejs app.js &, 执行任务服务程序 nodejs schedule.js

​      推荐采用pm2管理器对服务进行管理，详见：https://pm2.keymetrics.io/。

##  

## API 接口说明

​	本服务提供HTTP API接口，遵循RESTful 规则，采用JSON作为数据报文格式。 

####    1、信息上送：

​	    path： /api/info，method：POST   

​		参数： openId, temperature， mobile 等相关数据

​		示范： curl -H "Content-Type:application/json" -H "Data_Type:msg" -X POST --data '{"openId": "test000000001", "temperature": 38.7，  "mobile": "18000218888"}' http://127.0.0.1:8086/api/info

####    2、数据登记

​	    path：/api/note，method：POST

​		参数：openid，temperature，lon， lat 等相关数据

​        示范： curl -H "Content-Type:application/json" -H "Data_Type:msg" -X POST --data '{"openId": "test000000001", "temperature": 37.4，  "mobile": "18000218888", "lot":, 120.22212"lat": 30.32349}' http://127.0.0.1:8086/api/note



####    3、接受记录

​		为页面跳转的方式，处理完成之后，会重定向回到小程序

​        path：/web/record，method：GET  

​		参数：openId，lat，lot， code。。。。等信息

​		示范：curl -H "Content-Type:application/json" -H "Data_Type:msg" -X POST --data '{"openId": "test000000001", "lot":, 120.22212"lat": 30.32349，"code": "A237DE3234F214AC439"  }'  http://127.0.0.1:8088/api/login



## 更新日志